import './assets/main.css'

import { createApp } from 'vue'
import Specs from './views/Specs.vue'
import router from './router'

const specs = createApp(Specs)

specs.use(router)

specs.mount('#specs')
