import { createRouter, createWebHistory } from 'vue-router'
import Reqs from '../views/Reqs.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/Requirements',
      name: 'Requirements',
      component: Reqs
    }
  ]
})

export default router
